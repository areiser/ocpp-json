Small example on how to communicate using websockets with the OCPP standard programmed in Rust.

Used for a course on model based software development at MUAS.

Rust structs generated from json-schema using: [https://app.quicktype.io](https://app.quicktype.io)