/// Simple WebSocket server with error handling. It is not necessary to setup logging, but doing
/// so will allow you to see more details about the connection by using the RUST_LOG env variable.

extern crate ws;
extern crate env_logger;
extern crate serde;
 #[macro_use]
 extern crate serde_derive;
 #[macro_use]
 extern crate serde_json;

use ws::listen;
mod cancel_reservation_request;
mod cancel_reservation_response;
use cancel_reservation_response::CancelReservationResponse;
use cancel_reservation_response::Status;
use cancel_reservation_request::CancelReservationRequest;

struct OcppResponseMessage {

    messageType: u8,
    messageId: String,
    payload: String,

}

fn main () {

    // Setup logging
    env_logger::init();

    // Listen on an address and call the closure for each connection
    if let Err(error) = listen("127.0.0.1:3012", |out| {

        // The handler needs to take ownership of out, so we use move
        move |msg: ws::Message| {
            // Handle messages received on this connection
            println!("Server got message '{}'. ", msg);

            /*
                Example message:
                [
                    2,
                    "19223201",
                    "CancelReservationRequest",
                    {"reservationId":1}
                ]
            */

    
            let msg_string = msg.to_string();
            let mut parts = msg_string.split(",");
    
            //First: extract Message type
            let mut message_type = String::from(parts.next().unwrap());
            //Strip out ], spaces and newlines
            message_type.remove(0);
            message_type = message_type.replace(" ", "");
            message_type = message_type.replace("\n", "");
            println!("Message type {}", message_type);
            let message_type_nr: i64 = message_type.parse().unwrap();
            if message_type_nr != 2 {
                panic!("Unexpected message type!!");
            }

            let mut message_nr = String::from(parts.next().unwrap());
            //Strip out unwanted space, newlines and quotes
            message_nr = message_nr.replace(" ", "");
            message_nr = message_nr.replace("\n", "");
            message_nr = message_nr.replace("\"", "");
            println!("Message number: {}", message_nr);

            let mut message_action = String::from(parts.next().unwrap());
            //Strip out unwanted space, newlines and quotes
            message_action = message_action.replace(" ", "");
            message_action = message_action.replace("\n", "");
            message_action = message_action.replace("\"", "");
            println!("Message action: {}", message_action);

            if message_action != "CancelReservationRequest" {
                panic!("Unknown message action!");
            }

            let mut payload = String::from(parts.next().unwrap());
            let length = payload.len();
            //Remove last ]
            payload.remove(length - 1);
            println!("Payload: {}", payload);

            //Deserialize from json:
            let reservation_request: CancelReservationRequest = serde_json::from_str(&payload).unwrap();
            println!("Successfully deserialized, cancelling reservation for id: {}", reservation_request.reservation_id);

            //Send response
            let response = json!(CancelReservationResponse {
                status: Status::Accepted,
            });

            let reponseMessage = OcppResponseMessage {
                messageType: 3,
                messageId: message_nr.to_string(),
                payload: response.to_string(),
            };

            let requestMessageString = format!("[
                {},
                \"{}\",
                {}
            ]", reponseMessage.messageType, reponseMessage.messageId, reponseMessage.payload);
            
            // Use the out channel to send messages back
            out.send(requestMessageString)
        }

    }) {
        // Inform the user of failure
        println!("Failed to create WebSocket due to {:?}", error);
    }

}