
/// Simple WebSocket client with error handling. It is not necessary to setup logging, but doing
/// so will allow you to see more details about the connection by using the RUST_LOG env variable.

extern crate ws;
extern crate env_logger;
 #[macro_use]
 extern crate serde_derive;
 #[macro_use]
 extern crate serde_json;

use ws::{connect, CloseCode};
mod cancel_reservation_request;

struct OcppRequestMessage {

    messageType: u8,
    messageId: String,
    action: String,
    payload: String,
}

fn main () {

    // Setup logging
    env_logger::init();

    let request = json!(cancel_reservation_request::CancelReservationRequest {
        reservation_id: 1,
    });

    let requestMessage = OcppRequestMessage {
        messageType: 2,
        messageId: "19223201".to_string(),
        action: "CancelReservationRequest".to_string(),
        payload: request.to_string(),
    };

    let requestMessageString = format!("[
        {},
        \"{}\",
        \"{}\",
        {}
    ]", requestMessage.messageType, requestMessage.messageId, requestMessage.action, requestMessage.payload);

    // Connect to the url and call the closure
    if let Err(error) = connect("ws://127.0.0.1:3012", |out| {

        // Queue a message to be sent when the WebSocket is open
        if let Err(_) = out.send(requestMessageString.to_string()) {
            println!("Websocket couldn't queue an initial message.")
        } else {
            println!("Client sent message!")
        }

        // The handler needs to take ownership of out, so we use move
        move |msg| {

            // Handle messages received on this connection
            /*
            Should be something like 
                [
                    3,
                    "19223201",
                    {"status":"Accepted"}
                ]
            */
            println!("Client got message '{}'. ", msg);

            // Close the connection
            out.close(CloseCode::Normal)
        }

    }) {
        // Inform the user of failure
        println!("Failed to create WebSocket due to: {:?}", error);
    }

}